//
// Created by Strawberry on 2022/4/12.
//

#ifndef TEST_PAGETABLE_H
#define TEST_PAGETABLE_H

#include "Frame.h"
extern std::map<long long int, Page*> g_swap_space;
//存储全部页表
extern std::map<int,Frame> g_page_table;
//存储全部的帧
extern std::list<Frame*> g_all_frame;
//存储全部的空闲帧
extern std::list<Frame*> g_all_free_frame;

/**
 * PageTable
 * 页表
 * table 存储整个系统的页表,key为存储在页表的页号，value为对应的存储该页的空闲帧，在这里面的都是在已经在内存中的
 * swap_space_pages 这里面的page都是位于交换空间的，他们被调入过内存，但是由于swap out导致现在位于交换空间
 */
class PageTable {
public:
    std::map<int,Frame*> table;
    std::map<int,Page*> swap_space_pages;


    /**
     * 访问函数
     * @param logical_loc 要访问的逻辑地址
     * @return 该逻辑地址是否在内存中
     */
    bool access(long long logical_loc);

    /**
     * 判断给定逻辑地址是否在内存中
     * @param logical_loc
     * @return
     */
    bool in_memory(long long logical_loc);

    /**
     * 释放内存，将本页表中table的全部帧指向的页释放，然后将本页表中的全部帧设为空闲
     */
    void free();

    /**
     * 将指定逻辑地址对应的页换出内存，把那个帧空闲出来，然后把对应页放入交换空间
     * @param page 要交换的页面的指针
     */
    void swap_out(Page *page);

    /**
     * 去交换空间找到指定逻辑地址对应的页，然后将其调入页表
     * @param logical_loc
     */
    void swap_in(long long logical_loc);

    /**
     * 调页LRU算法，swap out一个页，然后将帧加入空闲帧列表
     */
    void lru();

    /**
     * 输出整个页表
     */
    void show();
};


#endif //TEST_PAGETABLE_H