//
// Created by Strawberry on 2022/4/12.
//

#ifndef TEST_MEMORYAPI_H
#define TEST_MEMORYAPI_H
#include "PageTable.h"
extern std::map<long long int, Page*> g_swap_space;
//存储全部页表
extern std::map<int,Frame> g_page_table;
//存储全部的帧
extern std::list<Frame*> g_all_frame;
//存储全部的空闲帧
extern std::list<Frame*> g_all_free_frame;

class MemoryApi {
public:
    /**
     * 访问内存
     * @param page_table 页表起始地址
     * @param logical_loc 要访问的逻辑地址
     * @return 访问该地址是否成功(目标地址是否已经被调入内存)
     */
    static bool access_memory(PageTable *page_table,long long logical_loc);

    /**
     * 查看目标地址是否已经被调入内存
     * @param page_table 页表起始地址
     * @param logical_loc 要访问的逻辑地址
     * @return 目标地址是否已经被调入内存
     */
    static bool is_in_memory(PageTable *page_table,long long logical_loc);

    /**
     * 获取当前内存中空闲帧的数目
     * @return 空闲帧的数目
     */
    static int get_free_frame_num();

    /**
     * 释放指定页表的内存
     * @param page_table 页表的起始地址
     */
    static void free_memory(PageTable *page_table);

    /**
     * 将指定逻辑地址对应的一页加载到内存
     * @param page_table 页表起始地址
     * @param logical_loc 目标逻辑地址
     */
    static void load_into_memory(PageTable *page_table,long long logical_loc);

    /**
     * 地址转换函数，将对应页的逻辑地址转换为物理地址
     * @param logical_loc 逻辑地址
     * @return 对应的物理地址
     */
    static long long transfer_location(Page *page,long long logical_loc);

    /**
     * 根据逻辑地址找到页号并返回
     * @param logical_loc 逻辑地址
     * @return 该逻辑地址的页号
     */
    static int get_page_number(long long logical_loc);

    /**
     * 输出全部空闲帧信息
     */
     static void show_all_free_frame();

     /**
      * 输出全部帧信息
      */
      static void show_all_frame();

      /**
       * 输出全部交换空间中的信息 
       */
      static void show_swap_space();

      /**
       * 初始化内存模块
       * @param page_frame_size 页大小/帧大小
       * @param frame_total_num 帧的总数
       */
      static void initial_memory(int page_frame_size,int frame_total_num);

      /**
       * 查看一个虚拟地址指向的页是否在交换空间中
       */
      static bool is_in_swap_space(long long logical_loc);

};


#endif //TEST_MEMORYAPI_H