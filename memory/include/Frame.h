//
// Created by Strawberry on 2022/4/12.
//

#ifndef TEST_FRAME_H
#define TEST_FRAME_H


#include <ostream>
#include "Page.h"

/**
 * Frame
 * 帧
 * page 该帧所对应存储的页的地址，指向所存储的页
 * is_free 该帧是否为空闲帧
 */
class Frame {
public:
    Page *page;
    bool is_free;
    int number;

    friend std::ostream &operator<<(std::ostream &os, const Frame &frame);

    Frame();

    Frame(Page *page, bool isFree, int number);
};






#endif //TEST_FRAME_H
