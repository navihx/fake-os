//
// Created by Strawberry on 2022/4/12.
//

#ifndef TEST_PAGE_H
#define TEST_PAGE_H

#include <ostream>
#include "header.h"


/**
 * Page
 * 页类
 * frame_number 该页在此页表中对应的帧号
 * page_number 该页在此页表中的页号，即该页是此页表中的第几页
 * is_in_memory 该页是否被调入了内存
 * access_flag 该页在内存中时，被LRU计算的标识(每次被访问则置0，否则加1)
 */

class Page {
public:
    int frame_number;
    int page_number;
    int access_flag;

    Page(int pageNumber,int frameNumber, int accessFlag);

    Page();

    static int get_page_number(long long logical_loc);

    friend std::ostream &operator<<(std::ostream &os, const Page &page);
};


#endif //TEST_PAGE_H
