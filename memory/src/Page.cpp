//
// Created by Strawberry on 2022/4/12.
//

#include "Page.h"

Page::Page(int pageNumber,int frameNumber, int accessFlag)
    : page_number(pageNumber),frame_number(frameNumber), 
      access_flag(accessFlag)
{
}



std::ostream &operator<<(std::ostream &os, const Page &page)
{
    os << "[frame_number: " << page.frame_number << ",page_number: " << page.page_number << ",is_in_memory: "
       << page.access_flag << "]";
    return os;
}

Page::Page() {}
