//
// Created by Strawberry on 2022/4/12.
//


#include "MemoryApi.h"
//存储交换空间
std::map<long long int, Page *> g_swap_space;
//存储全部页表
std::map<int, Frame> g_page_table;
//存储全部的帧
std::list<Frame *> g_all_frame;
//存储全部的空闲帧
std::list<Frame *> g_all_free_frame;

void MemoryApi::initial_memory(int page_frame_size,int frame_total_num){

    //初始化页/帧大小和帧数目
    g_frame_size = page_frame_size;
    g_frame_num = frame_total_num;
    g_offset_n = 0;
    int temp = g_frame_size >> 1;
    while (temp > 0)
    {
        g_offset_n++;
        temp = temp >> 1;
    }

    //先清理以前的数据
    g_all_frame.clear();
    g_all_free_frame.clear();

    //根据帧数来循环生成并添加
    for (int i = 1; i <= g_frame_num; i++) {
        auto frame = new Frame(nullptr, true, i);
        g_all_frame.push_back(frame);
        g_all_free_frame.push_back(frame);
    }

}

//TESTED
bool MemoryApi::access_memory(PageTable *page_table, long long int logical_loc) {
    return (*page_table).access(logical_loc);
}

int MemoryApi::get_free_frame_num() {
    return g_all_free_frame.size();
}

void MemoryApi::free_memory(PageTable *page_table) {
    (*page_table).free();
}

void MemoryApi::load_into_memory(PageTable *page_table, long long int logical_loc) {

    // std::cout << std::endl << "free = " << g_all_free_frame.size() << std::endl;
    // show_all_frame();
    // show_all_free_frame();
    if (get_free_frame_num() < 1) {
        //先LRU获得一个空闲帧
        // std::cout << "start lru" << std::endl;
        (*page_table).lru();
    }
    if(get_free_frame_num() >= 1  ){
        if ( g_swap_space.find(get_page_number(logical_loc)) == g_swap_space.end()) {
            // std::cout << "not in swap spawce" << std::endl;
            //如果交换空间中没有要调入的帧 说明其不在交换区 直接调入
            //从全部的空闲帧中获取一个帧
            auto frame = g_all_free_frame.front();
            int page_number = get_page_number(logical_loc);
            //构造所需页
            auto page = new Page(page_number,frame->number, 0);
            //把空闲帧指向该页
            frame->page = page;
            frame->is_free = false;
            //加入页表
            page_table->table[page->page_number] = frame;
            g_all_free_frame.remove(frame);

            //然后把页表中其他的已存入的页access_flag++
            std::vector<int> to_be_erased;
            for(const auto &item : page_table->table){
                if(item.first != page_number){
                    if(item.second != nullptr && item.second->page != nullptr){
                        item.second->page->access_flag++;
                    }else{
                        //发现为null的数据 说明当前帧应该为空闲 尝试修复
                        to_be_erased.push_back(page_number);
                    }
                }
            }
            //尝试修复
            for (const auto &item : to_be_erased){
                page_table->table.erase(item);
            }
        } else {
            //交换空间有需要调入的帧 则直接从交换空间拿了调入
            page_table->swap_in(logical_loc);
        }
    }
    
    // page_table->show();
    // show_all_frame();
    // show_all_free_frame();

}

bool MemoryApi::is_in_memory(PageTable *page_table, long long int logical_loc) {
    return (*page_table).in_memory(logical_loc);
}

//TESTED
long long MemoryApi::transfer_location(Page *page,long long logical_loc) {
    return ((page->frame_number << g_offset_n) + (logical_loc & ((1 << g_offset_n) - 1)));
}

//TESTED
int MemoryApi::get_page_number(long long int logical_loc) {
    return logical_loc >> g_offset_n;
;
}

//TESTED
void MemoryApi::show_all_free_frame() {
    std::cout << "free_frame_number\t\tfree_frame_info" << std::endl;
    for (const auto &item : g_all_free_frame){
        if(item != nullptr){
            std::cout << item->number << "\t\t" << *item << std::endl;
        }
    }
}

//TESTED
void MemoryApi::show_all_frame() {
    std::cout << "frame_number\t\tframe_info" << std::endl;
    for (const auto &item : g_all_frame){
        if(item != nullptr){
            std::cout << item->number << "\t\t" << *item << std::endl;
        }
    }
}

void MemoryApi::show_swap_space(){
    std::cout << "logical_loc\t\tframe_info" << std::endl;
    for(const auto &item : g_swap_space){
        if(item.second != nullptr){
            std::cout << item.first << "\t\t" << *(item.second) << std::endl;
        }
    }
}

bool MemoryApi::is_in_swap_space(long long logical_loc){
    return g_swap_space.find(get_page_number(logical_loc)) != g_swap_space.end();
}

#ifdef UNIT_TEST

#include <gtest/gtest.h>

/**
 * 初始化内存部分 
 * MemoryApi::initial_memory
 */
TEST(MEMORY_TEST, INITIAL_MEMORY_TEST)
{
    EXPECT_EQ(MemoryApi::get_free_frame_num(), g_frame_num);
    //4096的页大小对应12位
    EXPECT_EQ(g_offset_n, 12);
}

/**
 * 虚拟地址与物理地址转换
 * MemoryApi::transfer_location
 */
TEST(MEMORY_TEST, TRANSFER_LOCATION_TEST)
{
    //设定地址为
    long long a = 0b1010000000000001;
    //帧号为7 
    auto page = new Page(MemoryApi::get_page_number(a),7, 0);
    EXPECT_EQ(MemoryApi::transfer_location(page,a), 0x7001);
    a = 0xa001;
    EXPECT_EQ(MemoryApi::transfer_location(page,a), 0x7001);
    a = 40961;
    EXPECT_EQ(MemoryApi::transfer_location(page,a), 0x7001);

}

/**
 * 根据虚拟地址解析出他所在页表的哪一页
 * MemoryApi::get_page_number
 */
TEST(MEMORY_TEST, GET_PAGE_NUMBER_TEST)
{
    //设定地址为
    long long a = 0b1010000000000001;
    //偏移量为12位 故根据虚拟地址可以看到前4位代表所在页数 即12
    EXPECT_EQ(MemoryApi::get_page_number(a), 10);
}

/**
 * 测试LRU算法
 */
TEST(MEMORY_TEST, LRU_TEST)
{
    MemoryApi::initial_memory(1,3);

    PageTable pt;

    EXPECT_EQ(MemoryApi::is_in_memory(&pt,3),false);
    MemoryApi::load_into_memory(&pt,3);

    EXPECT_EQ(MemoryApi::is_in_memory(&pt,0),false);
    MemoryApi::load_into_memory(&pt,0);    

    EXPECT_EQ(MemoryApi::is_in_memory(&pt,2),false);
    MemoryApi::load_into_memory(&pt,2);

    EXPECT_EQ(MemoryApi::is_in_memory(&pt,1),false);
    MemoryApi::load_into_memory(&pt,1);

    EXPECT_EQ(MemoryApi::is_in_memory(&pt,3),false);
    
}
/**
 * 载入内存、访存、释放内存
 * MemoryApi::access_memory
 * MemoryApi::load_into_memory
 * MemoryApi::is_in_memory
 * MemoryApi::get_free_frame_num
 * MemoryApi::free_memory
 */
TEST(MEMORY_TEST, ACCESS_MEMORY_AND_LOAD_TEST)
{

    MemoryApi::initial_memory(4096,64);
    //设定地址为
    long long a = 0b1010000000000001;
    PageTable page_table;
    //初次访问时 未load入内存 故应该返回false
    EXPECT_EQ(MemoryApi::access_memory(&page_table, a), false);

    //然后load_into_memory
    MemoryApi::load_into_memory(&page_table, a);
    //已经load 应该返回true 且现在空闲帧已经被用了一个 应该剩63个
    EXPECT_EQ(MemoryApi::access_memory(&page_table, a), true);
    EXPECT_EQ(MemoryApi::get_free_frame_num(), 63);
    //然后再load一个 b
    long long b = 57346;
    MemoryApi::load_into_memory(&page_table, b);
    //然后再访问b 应该为true
    EXPECT_EQ(MemoryApi::access_memory(&page_table, b), true);
    //此时a的access_flag应该递增 为2(之前load b时也会递增一次)
    EXPECT_EQ(page_table.table[MemoryApi::get_page_number(a)]->page->access_flag, 2);
    //且空闲帧数目应该为62
    EXPECT_EQ(MemoryApi::get_free_frame_num(), 62);

    //此时a和b俩对应的页应该都在内存中
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table, a), true);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table, b), true);

    //然后free掉该page_table的全部内存
    MemoryApi::free_memory(&page_table);
    //此时a和b应该不在内存 且空闲帧的数目应该恢复64
    EXPECT_EQ(MemoryApi::get_free_frame_num(), 64);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table, a), false);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table, b), false);

    //接下来模拟内存不够时的情况
    //假设一共有两个空闲帧 但是要load三个页时

    //首先重置整个空闲帧列
    MemoryApi::initial_memory(4096,2);

    //现在应该有两个空闲帧
    EXPECT_EQ(MemoryApi::get_free_frame_num(), 2);

    //然后load a和b
    PageTable page_table2;
    MemoryApi::load_into_memory(&page_table2, a);

    MemoryApi::load_into_memory(&page_table2, b);
    //然后访问a 此时b的access_flag为1 如果LRU则他被换走
    MemoryApi::access_memory(&page_table2, a);

    EXPECT_EQ(page_table2.table[MemoryApi::get_page_number(a)]->page->access_flag, 0);
    EXPECT_EQ(page_table2.table[MemoryApi::get_page_number(b)]->page->access_flag, 1);

    //然后load c 此时应该把b换走 即b不在内存中了 a c在
    long long c = 0xf002;
    MemoryApi::load_into_memory(&page_table2, c);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table2, a), true);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table2, b), false);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table2, c), true);

    //先访问一次a 则下次load的时候根据LRU 被swap out的是c
    MemoryApi::access_memory(&page_table2, a);
    //此时b应该在交换空间 然后再去load b 时 应该从交换空间swap in 
    //首先看b是否在交换空间
    EXPECT_EQ(MemoryApi::is_in_swap_space(b), true);
    // MemoryApi::show_swap_space();   
    MemoryApi::load_into_memory(&page_table2, b);
    //现在b被swap in 进去了 他不在交换空间了 而且在内存了
    EXPECT_EQ(MemoryApi::is_in_swap_space(b), true);
    EXPECT_EQ(MemoryApi::is_in_memory(&page_table2,b), true);
    
}
#endif
