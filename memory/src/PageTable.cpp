//
// Created by Strawberry on 2022/4/12.
//

#include "PageTable.h"
int get_page_number(long long logical_loc){
    return logical_loc >> g_offset_n;
}
//TESTED
bool PageTable::access(long long int logical_loc) {
    if (in_memory(logical_loc)) {

        int page_number = get_page_number(logical_loc);
        table[page_number]->page->access_flag = 0;

        //把table里其他的page的access_flag加1
        //记住要erase的page_number
        std::vector<int> to_be_erased;
        for (const auto &item : table){
            if(item.first != page_number){
                if(item.second != nullptr && item.second->page != nullptr){
                    item.second->page->access_flag++;
                }else{
                    //发现为null的数据 说明当前帧应该为空闲 尝试修复
                    to_be_erased.push_back(page_number);
                }
            }
        }
        //尝试修复
        for (const auto &item : to_be_erased){
            table.erase(item);
        }
        return true;
    } else {
        return false;
    }
}



//TESTED
bool PageTable::in_memory(long long int logical_loc) {
    int page_number = get_page_number(logical_loc);
    // std::cout << "access " << page_number << std::endl;
    if(table.find(page_number) == table.end() && swap_space_pages.find(page_number) == swap_space_pages.end()){
        return false;
    }else {
        // return true;
        if(table.find(page_number) != table.end() ){
            return true;
        }
        //在交换空间中 也是没在内存 false
        else if(swap_space_pages.find(page_number) != swap_space_pages.end()){
            return false;
        }
    }
    return false;

     
}

void PageTable::free() {
    //记住要erase的page_number
    std::vector<int> to_be_erased;
    for (const auto &item: table) {
        if(item.second != nullptr){
            item.second->is_free = true;
            if(item.second->page != nullptr){
                int page_number = item.second->page->page_number;
                to_be_erased.push_back(page_number);
            }
            item.second->page = nullptr;
            g_all_free_frame.push_back(item.second);
        }
    }
    for (const auto &item : to_be_erased){
        table.erase(item);
    }
}

void PageTable::swap_out(Page *page) {
    //放到交换区
    g_swap_space[page->page_number] = page;
    //swap out时给他的access_number = 0
    // page->access_flag = 0;
    //释放它对应的帧
    table[page->page_number]->is_free = true;

    table[page->page_number]->page = nullptr;
    //加入到空闲帧list
    
    g_all_free_frame.push_back(table[page->page_number]);
    //从页表的table中删除该页
    //swap out不应该从页表中删除！
    table.erase(page->page_number);
    //转而放入页表的swap_space_pages
    swap_space_pages[page->page_number] = page;
    // show();
    
    #ifdef DEMO
    std::cout << "(swap out "<< page->page_number <<") ";
    #endif
    // std::cout << std::endl;
    // std::cout << "free after swap out = " << g_all_free_frame.size() << std::endl;
}

void PageTable::swap_in(long long int logical_loc) {
    //在交换区找这个逻辑地址对应的帧，然后加入到页表
    //找到一个空闲帧，并指向该页
    int page_number2 = get_page_number(logical_loc);
    auto frame = g_all_free_frame.front();
    frame->page = g_swap_space[page_number2];
    frame->page->access_flag = 0;
    frame->page->frame_number = frame->number;
    frame->page->page_number = page_number2;
    g_all_free_frame.remove(frame);
    #ifdef DEMO
    std::cout<< "(swap in " << page_number2 <<") ";
    #endif    
    frame->is_free = false;
    //放入页表
    table[page_number2] = frame;
    //从swap_space_pages删除
    swap_space_pages.erase(page_number2);

    //swap in之后给其他帧的access_flag +1
    //然后把页表中其他的已存入的页access_flag++
    std::vector<int> to_be_erased;
    for(const auto &item : table){
        if(item.first != page_number2){
            if(item.second != nullptr && item.second->page != nullptr){
                item.second->page->access_flag++;
            }else{
                //发现为null的数据 说明当前帧应该为空闲 尝试修复
                to_be_erased.push_back(page_number2);
            }
        }
    }
    //尝试修复
    for (const auto &item : to_be_erased){
        table.erase(item);
    }
}

void PageTable::lru() {
    //当前已有空闲帧 直接返回
    // std::cout << std::endl << "free = " << g_all_free_frame.size() << std::endl;
    // std::cout << std::endl;
    // show();
    if (g_all_free_frame.size() > 0) {
        return;
    } else {
        int max_access = 0;

        if (table.empty()) {
            return ;
        }

        auto to_be_swapped_out = table.begin()->second->page;
        for (const auto &item: table) {
            int now_access = item.second->page->access_flag;
            if (now_access > max_access) {
                max_access = now_access;
                to_be_swapped_out = item.second->page;
            }
        }
        if (to_be_swapped_out != nullptr) {
            // std::cout << "aaaaa" <<to_be_swapped_out->page_number << std::endl;
            swap_out(to_be_swapped_out);
        }else{
            // std::cout << "bbbbb" << std::endl;
        }
    }

}

//TESTED
void PageTable::show() {
    std::cout << "table:" << std::endl;
    std::cout << "page_number\t\tframe_info" << std::endl;

    for (const auto &item : table){
        if(item.second != nullptr){
            std::cout<< item.first << "\t\t" << *item.second << std::endl;
        }else{
            std::cout<< item.first << "\t\tnull" << std::endl;
        }

    }

    std::cout << "swap_space_pages" << std::endl;
    std::cout << "page_number\t\tpage_info" << std::endl;

    for (const auto &item : swap_space_pages){
        if(item.second != nullptr){
            std::cout<< item.first << "\t\t" << *item.second << std::endl;
        }else{
            std::cout<< item.first << "\t\tnull" << std::endl;
        }

    }
}
