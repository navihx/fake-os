#include "MemoryApi.h"
#include "PageTable.h"
#include "memory/shell.h"
#include "header.h"
#include <iostream>

void print_memory_info() {
    std::cout << "Page Size : " << g_page_size << std::endl
        << "Page Count : " << g_all_frame.size() << std::endl
        << "Free Page : " << g_all_free_frame.size() << std::endl
        << "MEM USAGE : " << double(g_all_free_frame.size()) / double(g_all_frame.size()) * 100.0 << "%" << std::endl;
}
