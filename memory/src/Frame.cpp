//
// Created by Strawberry on 2022/4/12.
//

#include "Frame.h"

std::ostream &operator<<(std::ostream &os, const Frame &frame) {
    if(frame.page != nullptr){
        os << "page: " << *frame.page << " is_free: " << frame.is_free << " number: " << frame.number;
        return os;
    }else{
        os << "page: null" << " is_free: " << frame.is_free << " number: " << frame.number;
        return os;
    }

}

Frame::Frame() {}

Frame::Frame(Page *page, bool isFree, int number) : page(page), is_free(isFree), number(number) {}
