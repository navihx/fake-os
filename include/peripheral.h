#ifndef __PERIPHERAL_H__
#define __PERIPHERAL_H__

#include <thread>
#include <string>
#include "sync-queue.hpp"
#include "interupt.hpp"

bool is__stopped();
void stop_cpu();

struct IORequest 
{
    long long arg1;
    long long arg2;
    long long arg3;
    long long arg4;

    enum Type
    {
        SHUTDOWN,
        REQUEST,
    } type;
};

class Peripheral
{
public:
    std::thread thread_handle;
    SyncQueue<IORequest> io_queue;

    virtual void run(std::shared_ptr<SyncQueue<Interupt>> interrupt_channel) = 0;

    int get_id() { return id; }
    void set_id(int new_id) { id = new_id; }

    const std::string& get_name() { return name; }
    void set_name(const std::string& new_name) { name = new_name; }

private:
    int id;
    std::string name;
};

class FCFSPeripheral : public Peripheral
{
public:
    virtual void run(std::shared_ptr<SyncQueue<Interupt>> interrupt_channel);
};

class SSTFPeripheral : public Peripheral
{
public:
    int seek_start=0;
    virtual void run(std::shared_ptr<SyncQueue<Interupt>> interrupt_channel);
};

#endif
