#ifndef __INTERUPT_H__
#define __INTERUPT_H__

struct Interupt
{
    long long arg1;
    long long arg2;
    long long arg3;
    long long arg4;

    // TODO add interrupt description
    enum Type
    {
        SHUT_DOWN,
        PAGE_FAULT,
        PAGE_READY,
        IO_ISSUE,
        IO_READY,
        TIME_OUT,
    } type;
};

#endif // !__INTERUPT_H__
