#ifndef __SEMAPHORE_H__
#define __SEMAPHORE_H__

#include <mutex>
#include <queue>

#define SEMAPHORE_COUNT 4

class Semaphore
{
public:
    Semaphore(int val=1) : val(val) {}
    void set_val(int v) { val = v; }

    /*
        @return 0成功，-1被挂起
     */
    int wait(int pid);

    /*
        @return 将被唤醒的pid
     */
    int release();


private:
    mutable std::mutex m;
    int val;
    std::queue<int> p_list;
};

extern Semaphore g_semaphore_array[SEMAPHORE_COUNT];

#endif