#ifndef __SHELL_H__
#define __SHELL_H__

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <tuple>
#include <vector>
#include "PCB.hpp"

using ShellCommand = std::function<void(std::vector<std::string>)>;

class Shell {
  public:
    /// constructor
    Shell(const std::vector<std::tuple<std::string, ShellCommand>>&);

    /// add a shell command dynamically
    /// @param name: 指令名
    /// @param func: 接受参数表的处理函数
    void add_command(const std::string& name, ShellCommand func);

    /// run shell
    void run();

  private:
    std::map<std::string, std::function<void(std::vector<std::string>)>>
        command_map;
};

void unimplemented(std::vector<std::string>);
void echo(std::vector<std::string> argv);
void imem(std::vector<std::string> argv);

// shell commands of module filesys
void ls(std::vector<std::string> argv);
void mkfile(std::vector<std::string> argv);
void cat(std::vector<std::string> argv);
void mkdir(std::vector<std::string> argv);
void chmod(std::vector<std::string> argv);
void idisk(std::vector<std::string> argv);
void ed(std::vector<std::string> argv);
void rm(std::vector<std::string> argv);

// exec helper
std::vector<Command> m_analysis_exec(const std::string &path);
void m_fill_command(Command &cd, std::string token);
bool m_check_command(const Command cd);
int m_all_is_num(std::string str);

#endif  // !__SHELL_H__
