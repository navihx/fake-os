#ifndef __CPU_H__
#define __CPU_H__

#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <memory>
#include <ostream>
#include <ratio>
#include <thread>
#include <vector>

#include "MemoryApi.h"
#include "PCB.hpp"
#include "PageTable.h"
#include "interupt.hpp"
#include "monitor.hpp"
#include "peripheral.h"
#include "scheduler.h"
#include "semaphores.h"
#include "sync-queue.hpp"

extern Monitor<int> memory_monitor;

class CPU {
  public:
    std::thread thread_handle;
    std::shared_ptr<SyncQueue<Interupt>> interupt_queue =
        std::make_shared<SyncQueue<Interupt>>();
    SyncQueue<Interupt>* global_interupt_queue = nullptr;

    template<class SCHEDULER_T>
    void
        run(std::shared_ptr<Monitor<SCHEDULER_T>>,
            std::vector<std::shared_ptr<Peripheral>> = {});

    int get_id() {
        return id;
    }
    void set_id(int new_id) {
        id = new_id;
    }

    int get_pid() {
        return pid;
    }
    void set_pid(int new_pid) {
        pid = new_pid;
    }

  private:
    int id;
    int pid = -1;
};

template<class SCHEDULER_T>
void CPU::run(
    std::shared_ptr<Monitor<SCHEDULER_T>> scheduler_ptr,
    std::vector<std::shared_ptr<Peripheral>> peripherals) {
    thread_handle = std::thread([this, &scheduler_ptr, peripherals]() {
        while (1) {
            std::this_thread::sleep_for(
                std::chrono::duration<int, std::milli>(500));

            // if no task running, get a task from scheduler
            if (this->get_pid() == -1) {
                auto scheduler_helper = scheduler_ptr->get_access();
                int pid = scheduler_helper->get_task();
                if (pid != -1) {
                    this->set_pid(pid);
                    scheduler_helper->set_run(pid);
                }
            }

            // run command from address pc
            if (this->get_pid() != -1) {
                int pid = get_pid();

                // Cannot fold operator-> for shared_ptr. Why?
                auto PCB_ptr = scheduler_ptr->get_access()->get_PCB(pid);
                if (!PCB_ptr) {
                    set_pid(-1);
                    goto PROCESS_INTERRUPT;
                }

                int pc = PCB_ptr->pc;
                PCB_ptr->pc = pc + 1;

                // TODO check page here
                if (!PCB_ptr->pt) {
                    PCB_ptr->pt = std::make_shared<PageTable>(PageTable());
                }

                if (!MemoryApi::access_memory(PCB_ptr->pt.get(), pc)) {
                    // issue PAGE_FAULT
                    if (global_interupt_queue == nullptr) {
                        interupt_queue->push(
                            Interupt {pid, pc, 0, 0, Interupt::PAGE_FAULT});
                    } else {
                        global_interupt_queue->push(
                            Interupt {pid, pc, 0, 0, Interupt::PAGE_FAULT});
                    }

                    scheduler_ptr->get_access()->set_wait(pid);
                    PCB_ptr->pc = pc;
                    set_pid(-1);
                    goto PROCESS_INTERRUPT;
                }

                if (pc >= PCB_ptr->commands.size() || pc < 0) {
                    scheduler_ptr->get_access()->exit(pid);
                    {
                        auto l = memory_monitor.get_access();
                        if (PCB_ptr->pt) {
                            MemoryApi::free_memory(PCB_ptr->pt.get());
                            PCB_ptr->pt.reset();
                        }
                    }
                    set_pid(-1);
                    goto PROCESS_INTERRUPT;
                }

                auto command = PCB_ptr->commands[pc];

                switch (command.type) {
                    case Command::print:
                        std::cout << command.sarg;
                        std::flush(std::cout);
                        break;

                    case Command::jump:
                        PCB_ptr->pc += command.arg2;
                        break;

                    case Command::wait:
                        if (command.arg1 < SEMAPHORE_COUNT
                            && command.arg1 > 0) {
                            int ret = g_semaphore_array[command.arg1].wait(
                                PCB_ptr->pid);
                            if (ret == -1) {
                                scheduler_ptr->get_access()->set_wait(
                                    PCB_ptr->pid);
                                this->pid = -1;
                            }
                        }
                        break;

                    case Command::signal:
                        if (command.arg1 < SEMAPHORE_COUNT
                            && command.arg1 > 0) {
                            int ret = g_semaphore_array[command.arg1].release();
                            if (ret != -1)
                                scheduler_ptr->get_access()->set_ready(ret);
                        }
                        break;

                    case Command::sleep:
                        scheduler_ptr->get_access()->set_ready(pid);
                        set_pid(-1);
                        break;

                    case Command::access:
                        if (!PCB_ptr->pt) {
                            goto PROCESS_INTERRUPT;
                        }

                        if (!MemoryApi::access_memory(
                                PCB_ptr->pt.get(),
                                command.arg1)) {
                            if (global_interupt_queue == nullptr) {
                                interupt_queue->push(Interupt {
                                    pid,
                                    command.arg1,
                                    0,
                                    0,
                                    Interupt::PAGE_FAULT});
                            } else {
                                global_interupt_queue->push(Interupt {
                                    pid,
                                    command.arg1,
                                    0,
                                    0,
                                    Interupt::PAGE_FAULT});
                            }

                            set_pid(-1);
                            PCB_ptr->pc = pc;
                            scheduler_ptr->get_access()->set_wait(pid);
                        }
                        break;

                    case Command::use:
                        if (global_interupt_queue == nullptr) {
                            // HACK: use rand arg4 for io address
                            srand((unsigned)time(NULL));
                            int tmp = rand();

                            interupt_queue->push(Interupt {
                                command.arg2,
                                command.arg1,
                                pid,
                                tmp,
                                Interupt::IO_ISSUE});
                        } else {
                            srand((unsigned)time(NULL));
                            int tmp = rand();

                            global_interupt_queue->push(Interupt {
                                command.arg2,
                                command.arg1,
                                pid,
                                tmp,
                                Interupt::IO_ISSUE});
                        }

                        scheduler_ptr->get_access()->set_wait(pid);
                        set_pid(-1);
                        break;

                    case Command::cacl:
                        // WARN: Do nothing here
                        // if PCB has status reg, cacl can set it's value
                        // just wait for a cycle
                        break;

                    case Command::quit:
                        scheduler_ptr->get_access()->exit(pid);
                        {
                            auto l = memory_monitor.get_access();
                            if (PCB_ptr->pt) {
                                MemoryApi::free_memory(PCB_ptr->pt.get());
                                PCB_ptr->pt.reset();
                            }
                        }
                        set_pid(-1);
                        break;

                    case Command::fork:
                        scheduler_ptr->get_access()->create(
                            pid,
                            PCB_ptr->commands,
                            PCB_ptr->pc);
                        break;

                    default:
                        // TODO not Implemented
                        // open
                        // write
                        // read
                        break;
                }
            }

            // process interrput

        PROCESS_INTERRUPT:
            auto interrupt = this->interupt_queue->try_pop();
            if (!interrupt && this->global_interupt_queue) {
                interrupt = this->global_interupt_queue->try_pop();
            }
            if (!interrupt) {
                continue;
            }

            switch (interrupt->type) {
                case Interupt::Type::SHUT_DOWN:
                    return;
                    break;

                case Interupt::Type::IO_ISSUE:
                    if (interrupt->arg2 < peripherals.size()) {
                        peripherals[interrupt->arg2]->io_queue.push(IORequest {
                            interrupt->arg1,
                            interrupt->arg2,
                            interrupt->arg3,
                            interrupt->arg4,
                            IORequest::REQUEST});
                    }
                    break;

                case Interupt::Type::IO_READY:
                    scheduler_ptr->get_access()->set_ready(interrupt->arg3);
                    break;

                case Interupt::Type::PAGE_FAULT: {
                    auto PCB_ptr =
                        scheduler_ptr->get_access()->get_PCB(interrupt->arg1);
                    if (!PCB_ptr || !PCB_ptr->pt)
                        break;

                    {
                        auto l = memory_monitor.get_access();

                        MemoryApi::load_into_memory(
                            PCB_ptr->pt.get(),
                            interrupt->arg2);
                        if (MemoryApi::access_memory(
                                PCB_ptr->pt.get(),
                                interrupt->arg2))
                            scheduler_ptr->get_access()->set_ready(
                                interrupt->arg1);
                        else {
                            if (global_interupt_queue == nullptr) {
                                interupt_queue->push(*interrupt);
                            } else {
                                global_interupt_queue->push(*interrupt);
                            }
                        }
                    }

                } break;

                case Interupt::Type::PAGE_READY:
                    // WARN:None of use now
                    break;

                case Interupt::Type::TIME_OUT: {
                    int pid = get_pid();
                    scheduler_ptr->get_access()->set_ready(pid);
                }
                    set_pid(-1);
                    break;

                default:
                    break;
            }
        }
    });
}

#endif
