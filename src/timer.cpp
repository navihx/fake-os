#include "timer.hpp"

#ifdef UNIT_TEST

#include <gtest/gtest.h>

TEST(TIMER_TEST, TIMEOUT_TEST)
{
    auto callback = std::make_shared<SyncQueue<Interupt>>();
    Timer t({callback});

    t.run(1000);

    EXPECT_EQ(callback->pop()->type, Interupt::Type::TIME_OUT);
    EXPECT_EQ(callback->pop()->type, Interupt::Type::TIME_OUT);
    EXPECT_EQ(callback->pop()->type, Interupt::Type::TIME_OUT);

    t.io_channel.push(IORequest{0,0,0,0,IORequest::Type::SHUTDOWN});
    t.thread_handle.join();
}

#endif