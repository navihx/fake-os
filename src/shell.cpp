#include "shell.h"

#include <exception>
#include <iostream>
#include <regex>
#include <string>
#include <tuple>

#include "filesys/shell.hpp"
#include "memory/shell.h"

Shell::Shell(
    const std::vector<std::tuple<std::string, ShellCommand>>& command_list) {
    for (const auto& it : command_list) {
        command_map[std::get<0>(it)] = std::get<1>(it);
    }
}

void Shell::add_command(const std::string& name, ShellCommand func) {
    command_map[name] = func;
}

std::vector<std::string> split_to_v(const std::string& s) {
    std::regex space("\\s+");
    return std::vector<std::string>(
        std::sregex_token_iterator(s.cbegin(), s.cend(), space, -1),
        std::sregex_token_iterator());
}

void Shell::run() {
    // run in a loop
    while (true) {
        std::cout << "$ ";
        std::cout.flush();

        std::string command_with_args;
        std::getline(std::cin, command_with_args);

        auto argv = split_to_v(command_with_args);

        if (argv.size() < 1) {
            continue;
        }

        int offset = (argv[0] == "" ? 1 : 0);

        if (offset == 1 && argv.size() < 2) {
            continue;
        }

        auto it = command_map.find(argv[0 + offset]);
        if (it != command_map.end()) {
            it->second(std::vector<std::string>(
                argv.begin() + 1 + offset,
                argv.end()));
        } else {
            std::cout << "Command " << argv[0 + offset] << " Not Found"
                      << std::endl;
        }
    }
}

void unimplemented(std::vector<std::string>) {
    std::cout << "unimplemented" << std::endl;
}

void echo(std::vector<std::string> argv) {
    for (const auto& it : argv) {
        std::cout << it << " ";
    }
    std::cout << std::endl;
}

void imem(std::vector<std::string> argv) {
    print_memory_info();
}

void ls(std::vector<std::string> argv) {
    for (const auto& path : argv) {
        int res = s_ls(path);
        if (res != 0) {
            std::cout << "Error Occurred" << std::endl;
        }
    }
}

void mkfile(std::vector<std::string> argv) {
    if (argv.size() != 2) {
        std::cout << "USAGE: mkfile <path> [RWX]+" << std::endl;
        return ;
    }

    auto path = argv[0];
    auto permission = argv[1];

    char permission_int = 0;

    for (auto c : permission) {
        switch (c) {
            case 'R':
            case 'r':
                permission_int |= 4;
                break;

            case 'W':
            case 'w':
                permission_int |= 2;
                break;

            case 'X':
            case 'x':
                permission_int |= 1;
                break;

            default:
                break;
        }
    }

    int res = s_mkfile(path, permission_int);
    if (res) {
        std::cout << "Error Occurred" << std::endl;
    }
}

void cat(std::vector<std::string> argv) {
    for (const auto& path : argv) {
        std::string buf;
        int res = s_cat(path, buf);
        if (res) {
            std::cout << "Error Occurred : " << path << std::endl;
            continue;
        }

        std::cout << buf << std::endl;
    }
}

void mkdir(std::vector<std::string> argv) {
    for (const auto& path : argv) {
        int res = s_mkdir(path);
        if (res) {
            std::cout << "Error Occurred" << std::endl;
        }
    }
}

void chmod(std::vector<std::string> argv) {
    if (argv.size() != 2) {
        std::cout << "USAGE: chmod <path> [RWX]+" << std::endl;
        return ;
    }

    auto path = argv[0];
    auto permission = argv[1];

    char permission_int = 0;

    for (auto c : permission) {
        switch (c) {
            case 'R':
            case 'r':
                permission_int |= 4;
                break;

            case 'W':
            case 'w':
                permission_int |= 2;
                break;

            case 'X':
            case 'x':
                permission_int |= 1;
                break;

            default:
                break;
        }
    }

    int res = s_chmod(path, permission_int);
    if (res) {
        std::cout << "Error Occurred" << std::endl;
    }
}

void idisk(std::vector<std::string> argv) {
    s_idisk();
}

void ed(std::vector<std::string> argv) {
    if (argv.size() != 2) {
        std::cout << "USAGE: ed <path> <delimiter>" << std::endl;
        return ;
    }

    auto path = argv[0];
    auto delimiter = argv[1];
    s_ed(path, delimiter);
}

void rm(std::vector<std::string> argv) {
    for (const auto& path : argv) {
        int res = s_rm(path);
        if (res) {
            std::cout << "Error Occurred : rm " << path << std::endl;
        }
    }
}

std::vector<Command> m_analysis_exec(const std::string &path)
{
    std::vector<Command> ans;
    std::string content;
    s_cat(path, content);
    std::stringstream ss(content);
    std::string token;
    int line = 0;
    while (std::getline(ss, token, '\n'))
    {
        Command temp;
        m_fill_command(temp, token);
        if (m_check_command(temp) == false)
        {
            temp.type = Command::error;
            temp.arg1 = line;
        }
        ans.push_back(temp);
        line++;
    }
    return ans;
}

void m_fill_command(Command &cd, std::string token)
{
    cd.arg1 = -1;
    cd.arg2 = -1;
    cd.sarg = "";
    std::string t_token;
    if (!token.empty())
    {
        token.erase(0, token.find_first_not_of(" "));
        token.erase(token.find_last_not_of(" ") + 1);
    }
    std::stringstream ss_token(token);
    int i = 0;
    while (std::getline(ss_token, t_token, ' '))
    {
        if (i == 0)
        {
            if (t_token == "print")
                cd.type = Command::print;
            else if (t_token == "wait")
                cd.type = Command::wait;
            else if (t_token == "signal")
                cd.type = Command::signal;
            else if (t_token == "use")
                cd.type = Command::use;
            else if (t_token == "open")
                cd.type = Command::open;
            else if (t_token == "write")
                cd.type = Command::write;
            else if (t_token == "read")
                cd.type = Command::read;
            else if (t_token == "cacl")
                cd.type = Command::cacl;
            else if (t_token == "quit")
                cd.type = Command::quit;
            else if (t_token == "sleep")
                cd.type = Command::sleep;
            else if (t_token == "jump")
                cd.type = Command::jump;
            else if (t_token == "fork")
                cd.type = Command::fork;
            else if (t_token == "access")
                cd.type = Command::access;
            else
                cd.type = Command::error;
        }
        else if (i == 1)
            cd.arg1 = m_all_is_num(t_token);
        else if (i == 2)
            cd.arg2 = m_all_is_num(t_token);
        else if (i == 3)
            cd.sarg = t_token;
        else
            cd.sarg = cd.sarg + " " + t_token;
        i++;
    }
}

bool m_check_command(const Command cd)
{
    if (cd.type == Command::error || cd.arg1 == -1 || cd.arg2 == -1)
        return false;
    return true;
}

int m_all_is_num(std::string str)
{
    for (int i = 0; i < str.size(); i++)
    {
        int tmp = (int)str[i];
        if (tmp >= 48 && tmp <= 57)
            continue;
        else
            return -1;
    }
    return atoi(str.c_str());
}

#ifdef UNIT_TEST

    #include <gtest/gtest.h>

TEST(SHELL_TEST, SPLIT_TEST) {
    std::string test_string = "let us test spliting";
    std::vector<std::string> expect = {"let", "us", "test", "spliting"};
    auto res = split_to_v(test_string);

    for (int i = 0; i < expect.size(); i++) {
        ASSERT_STREQ(res[i].c_str(), expect[i].c_str());
    }
}

#endif  // UNIT_TEST
