#include <exception>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "MemoryApi.h"
#include "PCB.hpp"
#include "config.h"
#include "cpu.hpp"
#include "cxxopts.hpp"
#include "filesys/shell.hpp"
#include "monitor.hpp"
#include "peripheral.h"
#include "scheduler.h"
#include "shell.h"
#include "sync-queue.hpp"
#include "timer.hpp"
#include "vfs.hpp"

int main(int argc, char* argv[]) {
#ifdef UNIT_TEST

    // force `ld` link `LIB_TEST`
    Scheduler* scheduler_ptr = new FCFSScheduler(64);
    MemoryApi::initial_memory(4096, 64);
    testing::InitGoogleTest();
    return RUN_ALL_TESTS();

#else

    std::cout << "Fake-OS" << std::endl << "Version " << VERSION << std::endl;

    // TODO: command line args parse and init setting

    cxxopts::Options option("fake-os", "A simple OS simulator");
    option.add_options()(
        "t,time",
        "time slice length",
        cxxopts::value<int>()->default_value("10000"))(
        "c,core",
        "cpu count",
        cxxopts::value<int>()->default_value("1"))(
        "f,fs",
        "filesys image path",
        cxxopts::value<std::string>()->default_value("fs.image"))(
        "p,peripheral",
        "peripherals setting, S : SSTF, F : FCFS",
        cxxopts::value<std::string>()->default_value("SF"))(
        "s,pagesize",
        "page size",
        cxxopts::value<int>()->default_value("1024"))(
        "C,pagecount",
        "page count",
        cxxopts::value<int>()->default_value("1024"))(
        "P,ready",
        "ready queue length",
        cxxopts::value<int>()->default_value(
            "0"))("h,help", "Print help message");

    auto args = option.parse(argc, argv);

    if (args["help"].count()) {
        std::cout << option.help() << std::endl;
        return 0;
    }

    // TODO: kernel init
    auto scheduler_ptr =
        std::make_shared<Monitor<FCFSScheduler>>(args["ready"].as<int>());
    MemoryApi::initial_memory(
        args["pagesize"].as<int>(),
        args["pagecount"].as<int>());
    int res = fs.fs_init(args["fs"].as<std::string>());
    if (res) {
        std::cout << "Filesys init error" << std::endl;
        return 0;
    }

    // TODO: setup cpu, timer and peripherals
    auto global_interrupt_ptr = std::make_shared<SyncQueue<Interupt>>();

    std::vector<std::shared_ptr<Peripheral>> peripheral_ptrs = {
        //    std::make_shared<FCFSPeripheral>(),
        //    std::make_shared<SSTFPeripheral>()
    };

    for (const auto p : args["peripheral"].as<std::string>()) {
        switch (p) {
            case 'S':
                peripheral_ptrs.push_back(std::make_shared<SSTFPeripheral>());
                break;

            case 'F':
                peripheral_ptrs.push_back(std::make_shared<FCFSPeripheral>());
                break;
        }
    }

    int i = 0;
    for (auto p : peripheral_ptrs) {
        p->set_id(i);
        p->set_name("dev");
        p->run(global_interrupt_ptr);
        i++;
    }

    std::vector<std::shared_ptr<CPU>> cpu_ptrs = {};
    Timer timer({});
    for (int i = 0; i < args["core"].as<int>(); i++) {
        cpu_ptrs.push_back(std::make_shared<CPU>());
    }

    i = 0;
    for (auto it : cpu_ptrs) {
        it->set_id(i);
        it->global_interupt_queue = global_interrupt_ptr.get();
        timer.interrupt_channels.push_back(it->interupt_queue);
        it->run(scheduler_ptr, peripheral_ptrs);
        i++;
    }
    timer.run(args["time"].as<int>());

    // TODO: setup and run shell

    // exec closure
    ShellCommand execute = [scheduler_ptr](std::vector<std::string> argv) {
        if (argv.size() != 1) {
            std::cout << "USAGE : exec <executable path>" << std::endl;
            return;
        }

        auto commands = m_analysis_exec(argv[0]);

        if (commands.size() == 0 || commands[0].type == Command::error) {
            std::cout << "Parse executable error" << std::endl;
            return;
        }

        scheduler_ptr->get_access()->create(0, commands, 0);
        return;
    };

    // ps closure
    ShellCommand ps = [scheduler_ptr](std::vector<std::string> argv) {
        std::cout << "PID\tSTATE\tPPID\tPC" << std::endl;

        std::string state;

        auto sp = scheduler_ptr->get_access();
        for (const auto& p : sp->PCB_table) {
            const auto& pcb = p.second;

            switch (pcb->state) {
                case PCB::NEW:
                    state = "NEW";
                    break;

                case PCB::READY:
                    state = "READY";
                    break;

                case PCB::WAIT:
                    state = "WAIT";
                    break;

                case PCB::RUN:
                    state = "RUN";
                    break;

                default:
                    state = "UNKNOWN";
                    break;
            }

            std::cout << pcb->pid << "\t" << state << "\t" << pcb->ppid << "\t"
                      << pcb->pc << "\t" << std::endl;
        }
    };

    // kill closure
    ShellCommand kill = [scheduler_ptr](std::vector<std::string> argv) {
        if (argv.size() < 1) {
            std::cout << "Need an arg" << std::endl;
        }

        for (const auto& s : argv) {
            try {
                int pid = std::stoi(s);
                auto PCB = scheduler_ptr->get_access()->get_PCB(pid);
                if (!PCB) {
                    std::cout << "Cannot kill " << s << std::endl;
                    continue;
                }
                scheduler_ptr->get_access()->kill(pid);
                {
                    auto l = memory_monitor.get_access();
                    if (PCB->pt) {
                        MemoryApi::free_memory(PCB->pt.get());
                        PCB->pt.reset();
                    }
                }
                std::cout << pid << " Stopped" << std::endl;
            } catch (std::exception e) {
                std::cout << "Cannot kill " << s << std::endl;
            }
        }
    };

    // pmem closure
    ShellCommand pmem = [&](std::vector<std::string> argv) {
        for (const auto& sid : argv) {
            try {
                int pid = std::stoi(sid);
                auto PCB = scheduler_ptr->get_access()->get_PCB(pid);
                if (!PCB || !PCB->pt) {
                    continue;
                }

               PCB->pt->show();
            } catch (std::exception e) {
                continue;
            }
        }
    };

    std::vector<std::tuple<std::string, ShellCommand>> shell_commands = {
        // add shell commands below
        // std::make_tuple("", unimplemented)
        // some of the commands use some local variable, so we need to use a closure
        std::make_tuple("echo", echo),
        std::make_tuple("ls", ls),
        std::make_tuple("mkfile", mkfile),
        std::make_tuple("cat", cat),
        std::make_tuple("mkdir", mkdir),
        std::make_tuple("chmod", chmod),
        std::make_tuple("exec", execute),
        std::make_tuple("ed", ed),
        std::make_tuple("rm", rm),
        std::make_tuple("ps", ps),
        std::make_tuple("kill", kill),
        std::make_tuple("imem", imem),
        std::make_tuple("pmem", pmem),
        std::make_tuple("idisk", idisk)};

    Shell shell(shell_commands);

    ShellCommand help_command = [&](std::vector<std::string> argv) {
        std::cout << "available commands:" << std::endl;
        for (auto tuple : shell_commands) {
            std::cout << std::get<0>(tuple) << std::endl;
        }
    };

    ShellCommand shutdown_command = [&](std::vector<std::string> argv) {
        // shutdown all cpu
        for (auto p : cpu_ptrs) {
            p->interupt_queue->push(Interupt {0, 0, 0, 0, Interupt::SHUT_DOWN});
            if (p->thread_handle.joinable())
                p->thread_handle.join();
        }

        // shutdown all peripherals
        for (auto p : peripheral_ptrs) {
            p->io_queue.push(IORequest {0, 0, 0, 0, IORequest::SHUTDOWN});
            if (p->thread_handle.joinable())
                p->thread_handle.join();
        }

        timer.io_channel.push(IORequest {0, 0, 0, 0, IORequest::SHUTDOWN});

        fs.fs_shutdown();
        std::cout << "Bye~" << std::endl;

        exit(0);
    };

    shell.add_command("help", help_command);
    shell.add_command("shutdown", shutdown_command);

    shell.run();

    return 0;

#endif
}

#ifdef UNIT_TEST

TEST(NAIVE_TEST, PLUS) {
    ASSERT_EQ(1 + 2, 3);
}

#endif
