#include "semaphores.h"
Semaphore g_semaphore_array[SEMAPHORE_COUNT];
int Semaphore::wait(int pid)
{
    std::lock_guard<std::mutex> lk(m);
    val--;

    if(val < 0)
    {
        p_list.push(pid);
        return -1;
    }

    return 0;
}

int Semaphore::release()
{
    std::lock_guard<std::mutex> lk(m);
    val++;

    if(val <= 0)
    {
        int p = p_list.front();
        p_list.pop();
        return p;
    }

    return -1;
}
