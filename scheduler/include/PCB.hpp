#ifndef __PCB_HPP__
#define __PCB_HPP__

#include <memory>
#include <string>
#include <vector>

#include "MemoryApi.h"
#include "PageTable.h"

struct Command
{
    enum Type
    {
        print,  // 输出sarg到标准输出
        wait,   // wait信号量arg1
        signal, // signal信号量arg1
        use,    // 使用arg1设备arg2时间
        open,   // 打开文件sarg
        write,  // 写arg1 fd对应文件sarg
        read,   // 读arg1 fd对应文件
        cacl,   // 计算arg2周期
        access, // 读写arg1地址内存
        quit,   // 结束
        sleep,  // 返回ready
        jump,   // 无条件跳转偏移arg2
        fork,   // Fork
        error   // Error 
    } type;

    int arg1;
    int arg2;
    std::string sarg;
};

struct PCB
{
    int ppid;
    int pid;

    enum State
    {
        NEW,
        READY,
        RUN,
        WAIT,
    } state;

    std::shared_ptr<PageTable> pt;
    // FileDescriptorTable* fdt;
    int pc;
    std::vector<Command> commands;

    struct Priority
    {
        int p;   //  数越小，优先级越高，0为最高优先级
    } priority;
};

#endif
