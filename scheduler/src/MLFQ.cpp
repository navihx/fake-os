#include "scheduler.h"

int MLFQScheduler::create(int ppid, const std::vector<Command> &commands, int pc)
{
    int pid = -1;
    for (int i = 1; i < 65536; i++)
    {
        if (PCB_table.find(i) == PCB_table.end())
        {
            pid = i;
            break;
        }
    }
    if (pid == -1)
        return -1;

    auto PCB_ptr = std::make_shared<PCB>(PCB{
        ppid,
        pid,
        PCB::NEW,
        nullptr,
        pc,
        commands,
        PCB::Priority{
            0}});

    PCB_table.insert({pid, PCB_ptr});
    new_queue.push_back(pid);

    return pid;
}

int MLFQScheduler::admit(int pid)
{
    auto it = new_queue.begin();
    bool found = false;

    for (; it != new_queue.end(); it++)
    {
        if (*it == pid)
        {
            found = true;
            break;
        }
    }

    if (!found)
    {
        return -1;
    }

    ready_queue0.push_back(*it);
    new_queue.erase(it);
    return 0;
}

int MLFQScheduler::get_task()
{
    int size = ready_queue0.size() + ready_queue1.size() + ready_queue2.size() + ready_queue3.size();
    while (!new_queue.empty() && (max_progress_count <= 0 || size < max_progress_count))
    {
        int pid = new_queue.front();
        ready_queue0.push_back(pid);
        new_queue.pop_front();
    }

    if (size == 0)
        return -1;

    if (ready_queue0.size() != 0)
        return ready_queue0.front();
    else if (ready_queue1.size() != 0)
        return ready_queue1.front();
    else if (ready_queue2.size() != 0)
        return ready_queue2.front();
    else
        return ready_queue3.front();
}

int MLFQScheduler::set_run(int pid)
{
    auto it = ready_queue0.begin();
    bool found = false;

    for (; it != ready_queue0.end(); it++)
    {
        if (*it == pid)
        {
            found = true;
            break;
        }
    }
    if (found)
        ready_queue0.erase(it);
    else
    {
        for (it = ready_queue1.begin(); it != ready_queue1.end(); it++)
        {
            if (*it == pid)
            {
                found = true;
                break;
            }
        }
        if (found)
            ready_queue1.erase(it);
        else
        {
            for (it = ready_queue2.begin(); it != ready_queue2.end(); it++)
            {
                if (*it == pid)
                {
                    found = true;
                    break;
                }
            }
            if (found)
                ready_queue2.erase(it);
            else
            {
                for (it = ready_queue3.begin(); it != ready_queue3.end(); it++)
                {
                    if (*it == pid)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return -1;
                
                ready_queue3.erase(it);
            }
        }
    }

    PCB_table[pid]->state = PCB::RUN;
    run_queue.push_back(pid);

    return 0;
}

int MLFQScheduler::set_ready(int pid)
{
    auto it = wait_queue.begin();
    bool found = false;

    for (; it != wait_queue.end(); it++)
    {
        if (*it == pid)
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        wait_queue.erase(it);
        PCB_table[pid]->state = PCB::READY;
        ready_queue0.push_back(pid);
        PCB_table[pid]->priority.p = 0;
        return 0;
    }

    it = run_queue.begin();
    found = false;

    for (; it != run_queue.end(); it++)
    {
        if (*it == pid)
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        run_queue.erase(it);
        PCB_table[pid]->state = PCB::READY;
        switch (PCB_table[pid]->priority.p)
        {
            case 0:
                PCB_table[pid]->priority.p = 1;
                ready_queue1.push_back(pid);
                break;
            case 1:
                PCB_table[pid]->priority.p = 2;
                ready_queue2.push_back(pid);
                break;
            case 2:
                PCB_table[pid]->priority.p = 3;
                ready_queue3.push_back(pid);
                break;
            case 3:
                ready_queue3.push_back(pid);
                break;
            default:
                break;     
        }
        return 0;
    }

    return -1;
}

int MLFQScheduler::set_wait(int pid)
{
    auto it = run_queue.begin();
    bool found = false;

    for (; it != run_queue.end(); it++)
    {
        if (*it == pid)
        {
            found = true;
            break;
        }
    }

    if (!found)
    {
        return -1;
    }

    run_queue.erase(it);

    PCB_table[pid]->state = PCB::WAIT;
    run_queue.push_back(pid);

    return 0;
}

int MLFQScheduler::exit(int pid)
{
    return kill(pid);
}

int MLFQScheduler::kill(int pid)
{
    PCB_table.erase(pid);

    for (auto it = new_queue.begin(); it != new_queue.end(); it++)
    {
        if (*it == pid)
        {
            new_queue.erase(it);
            return 0;
        }
    }
    for (auto it = ready_queue0.begin(); it != ready_queue0.end(); it++)
    {
        if (*it == pid)
        {
            ready_queue0.erase(it);
            return 0;
        }
    }
    for (auto it = ready_queue1.begin(); it != ready_queue1.end(); it++)
    {
        if (*it == pid)
        {
            ready_queue1.erase(it);
            return 0;
        }
    }
    for (auto it = ready_queue2.begin(); it != ready_queue2.end(); it++)
    {
        if (*it == pid)
        {
            ready_queue2.erase(it);
            return 0;
        }
    }
    for (auto it = ready_queue3.begin(); it != ready_queue3.end(); it++)
    {
        if (*it == pid)
        {
            ready_queue3.erase(it);
            return 0;
        }
    }
    for (auto it = run_queue.begin(); it != run_queue.end(); it++)
    {
        if (*it == pid)
        {
            run_queue.erase(it);
            return 0;
        }
    }
    for (auto it = wait_queue.begin(); it != wait_queue.end(); it++)
    {
        if (*it == pid)
        {
            wait_queue.erase(it);
            return 0;
        }
    }

    return 0;
}

std::shared_ptr<PCB> MLFQScheduler::get_PCB(int pid)
{
    decltype(PCB_table)::iterator it;
    if ((it = PCB_table.find(pid)) != PCB_table.end())
    {
        return it->second;
    }
    else
    {
        return nullptr;
    }
}

#ifdef UNIT_TEST

#include <gtest/gtest.h>

TEST(SCHEDULER_TEST, MLFQ_TEST)
{
    Scheduler *s = new MLFQScheduler();

    int pid[4];

    for (int i = 0; i < 4; i++)
        pid[i] = s->create(0, {}, 0);

    for (int i = 0; i < 4; i++)
    {
        EXPECT_EQ(pid[i], s->get_task());
        s->set_run(pid[i]);
    }
}

#endif
