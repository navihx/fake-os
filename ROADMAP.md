# 路线图

## 模块

```
                     +--------+      +-------+
                     |        |      |       |
                     | Kernel +----->| Shell |
                     |        |      |       |
                     +---+----+      +-------+
                         |
                         |
                         |
                         |
        +----------------+------------------+
        |                |                  |
        |                |                  |
        v                v                  v
+---------------+  +-------------+    +-----------+
|               |  |             |    |           |
| MemoryManager |  | FileManager |    | Scheduler |
|               |  |             |    |           |
+---------------+  +-------------+    +-----------+
```

### 内核初始化

- 从json文件读取虚拟机配置和OS初始化设置
- 初始化文件系统
- 初始化页表
- 初始化外设
- 启动调度器
- 启动Shell

### 内存管理

- 分配,回收,整理内存
- 内存分配算法

### 中断处理

- 模拟定时器中断,IO中断

### 进程管理

- PCB结构设计
- 实现调度器
- 进程创建与销毁,模拟内存申请和释放
- 进程休眠与唤醒
- 进程调度算法(长期,中期,短期调度)

### 磁盘管理与文件系统

- FCB结构设计
- 树形目录结构
- 创建,删除,读取文件,管理文件权限
- 模拟磁盘,磁盘管理算法

### Shell

- 读取并解析命令
- 系统管理的程序(见下表)

|程序名称|用途|用法|
|-|-|-|
|help|显示所有指令的帮助信息|`$ help`|
|snapshot|绘制并保存系统信息图表(内存分配情况,磁盘使用情况,绘制进程运行甘特图)|`$ snapshot`|
|imem|展示内存使用信息,页表/段表,swap使用情况|`$ imem`|
|idisk|展示(用于存储文件的)磁盘使用情况|`$ idisk`|
|mkfile|创建新文件|`$ mkfile FILE RWX`|
|cat|输出文件内容|`$ cat FILE`|
|dd|输入文件(重新计算文件大小并存储到外存)|`$ dd FILE`,跟随提示输入文件内容|
|mkdir|创建新目录|`$ mkdir DIR`|
|chmod|更改文件权限|`$ chmod FILE RWX`|
|exec|执行某个有执行权限文件|`$ exec FILE`|
|ps|展示进程快照|`$ ps`|
|kill|杀死一个进程|`$ kill PID`|

## 流程

```
                                        +-------------------+
                     +-------+          |                   |
                     | Shell |          |  Peripherals Sim  |
                     +-------+          |                   |
                          ^             +-------------------+
                          |                     ^
                          |                     |
                          |                Spawn thread
                          |                     |
                          |                     |
                   +------+------+              |                                   +---------+
     Startup       |             |              |                                   |         |
      ------------>| Kernel init +--------------+-------Spawn threads----+--------->| CPU Sim |
                   |             |                                       |          |         |
                   +-------------+                                       |          +---------+
                 xxx             xxx                                     |
                xx                 xx                                    |               .
               xx   Init Monitor    xx                                   .               .
              xx                     xx                                  .               .
                                                                         .
       +------------+ +--------------+ +---------+                       |
       |File Manager| |Memory Manager| |Scheduler|                       |          +---------+
       +------------+ +--------------+ +---------+                       |          |         |
                                                                         +--------->| CPU Sim |
                                                                                    |         |
                                                                                    +---------+






     CPU Sim Loop:    |
                      |
   +------------------+-----------------------------------------+
   |                  |                                         |
   |                  v                                         |
   |         Get a new job from scheduler                       |
   |               (May block)                                  |
   |                  | <---------------------------------------x---+
   |                  |                                         |   |
iss|e IO  interrupt?  v                                         |   |
   |       +------exec code                                     |   |
   |       |          |                                         |   |
   |       |          |                                         |   |
   |       |          v                                         |   |
   |       |      process interrupt                             |   |
   |  set WAITING     |                                         |   |
   |       |          |                                         |   |
   |       |          v                                         |   |
   +-------+      task done?---YES----->release resource--------+   |
                      |                                         |   |
                      |                                         |   |
                      v                                         |   |
                  time up?-----YES----->set READY---------------+   |
                      |                                             |
                      |                                             |
                      |                                             |
                      +---------------------------------------------+
```

## 指令

### fork

复制当前进程并运行.设置新的进程id与父进程id.  
可能因内存不足创建失败

### acess

```
acess [memory size]
```

向memory manager请求内存.在请求过程中会锁住memory manager.  
可能因内存不足失败

### cacl

```
cacl [clock]
```

模拟计算指定周期数

### use

```
use [dev num] [time]
```

请求使用某个设备指定时间并阻塞进程至请求完成.

### quit

结束进程运行并释放资源.  
运行到程序结尾会默认执行quit.

### write / read

```
write / read [path] [time]
```

写文件/读文件指定周期数,为文件加上指定互斥/共享锁直到IO完成(并不发生真正的读写)  
可能会等待获取权限  
可能因path错误或不存在而出错

## 模拟外设与模拟中断

当CPU执行use指令时,将模拟IO请求发送到IO请求消息队列中.
外设从消息队列接受请求,完成请求(即为该请求服务制定的时间)后发送中断请求到中断消息队列等待CPU完成中断服务.

消息队列是实现了线程间同步的支持多生产者多消费者队列,实现细节见`include/sync-queue.hpp`.
消息队列提供的API与STL queue类似,提供了额外的try_pop非阻塞pop方法

## 内核模块

每一个模块都是一个monitor,意味着同时最多只能有一个调用者可以进行操作,其他调用者需阻塞至前一个调用者推出后才能进入.  
monitor的实现细节见`include/monitor.hpp`.monitor的使用类似智能指针,参考如下

```cpp
// foo类实例将被monitor托管,提供了bar方法
class Foo {
    void bar();
}

// 托管一个foo类实例
Monitor<foo> foo_monitor;

// 可以将Monitor直接视为指针进行操作
foo_monitor->bar();

// 获取一个被托管实例的指针,在指针销毁前锁定Monitor
{
    auto handle = foo_monitor.get_access();
    handle->bar();
}

// 获取被托管实例的引用但不锁定Monitor
// 线程不安全!!!
auto& ref=foo_monitor.unsafe_get_access();
ref.bar();
```

### 文件管理 FileManager

提供文件管理的API

用本地的目录进行模拟

|API|Usage|
|-|-|
|构造函数 (blocksize, tracks, sections， root)|从`root`目录建立模拟文件系统|
|dir_list(path)|列出`path`目录的内容|
|create_dir(path)|创建目录|
|remove_dir(path)|删除目录|
|chmod_file(path,rwx)|更改权限|
|create_file(path,rwx)|创建文件|
|remove_file(path)|删除文件|
|read_file(path)|获得`path`文件的内容|
|write_file(path)|向`path`文件写入内容|
|share_lock(path)| - |
|mutex_lock(path)| - |

### 内存管理 MemoryManager

提供内存管理的API

模拟分配内存与交换,控制内存与交换空间

|API|Usage|
|-|-|
|access(pid,size)| - |
|free(pid)| - |

### 调度器 Scheduler

提供进程调度的API

|API|Usage|
|-|-|
|create(path)| - |
|wake(pid)| - |
|sleep(pid)| - |
|wait(pid)| - |
|kill(pid)| - |
|die(pid)| - |

## 中断与IO请求

每种中断与IO请求都有4个参数，为`argx`

中断
|Type|Description|arg1|arg2|arg3|arg4|
|-|-|-|-|-|-|
SHUT_DOWN|关机信号|-|-|-|-|
PAGE_FAULT|缺页中断|缺页的进程pid|地址|Reserved|Reserved|
PAGE_READY|调页完成，将进程唤醒|pid|Reserved|Reserved|Reserved|
IO_ISSUE|发出IO请求中断，进程休眠|使用设备时间|设备号|进程pid|与IO请求相关的参数如磁道号等|
IO_READY|IO请求完成，唤醒进程|Reserved|设备号|进程pid|与IO请求相关的参数如磁道号等|

IO请求
|Type|Description|arg1|arg2|arg3|arg4|
|-|-|-|-|-|-|
SHUT_DOWN|关机信号|-|-|-|-|
REQUEST|IO请求|使用设备时间|设备号|进程pid|与IO请求相关的参数如磁道号等|

## 测试

使用`google test`进行单元测试.单元测试代码条件编译需要`UNIT_TEST`宏,创建单元测试按照如下规范 

```cpp
#ifdef UNIT_TEST // 检查UNIT_TEST宏

TEST(${MODULE_NAME}_TEST, ${TEST_NAME})
{
    // do some thing here
    ASSERT_EQ(1+2, 3);
}

#endif
```

> 注意: 编译调试版本将会使用DEBUG宏与UNIT_TEST宏,调试版本生成两个可执行文件`fake-os`和`unit_tests`,UNIT_TEST条件编译的代码仅会出现在可执行文件`unit_test`,在Release版本中没有这两个宏.

## 构建

使用`cmake`进行构建

依赖

|dependency|version|optional|
|-|-|-|
|[google test](https://github.com/google/googletest)|1.11.0-3|✔|

## 计划表

1-3周: 完成需求分析,总体设计与成员分工,制定测试计划  
4-5周: 完善总体设计,完成详细设计,设计测试用例  
6-9周: 根据分工完成代码编写,并同步进行单元测试与Debug  
10-11周: 完成系统测试,撰写文档与报告  

## 分工

黄幸:  设计,同步机制代码编写,内核启动初始化代码编写,Shell实现  
王振浩: 文件管理实现,硬盘空间管理  
王宏宇: 内存管理API实现,交换空间管理  
李佳叡: 调度器实现,Shell实现  
江炫征: 文件管理实现,FCB设计,文件读写控制  
陈默涵: 调度器实现,测试工作  
