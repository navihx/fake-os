#include "PCB.hpp"
#include "cpu.hpp"
#include "MemoryApi.h"
#include "scheduler.h"
#include <memory>
#include <string>
#include <vector>

int main() {
    // setup cpu
    CPU cpu;
    cpu.set_id(1);

    // setup memory module
    MemoryApi::initial_memory(1024, 2048);

    // setup commands;
    std::vector<Command> commands;
    for (int i=0;i< 1024*9;i++) {
        commands.push_back({Command::access, i, 0, ""});
        commands.push_back({Command::print, 0, 0, "access " + std::to_string(i) + "\n"});
    }
    commands.push_back({Command::print, 0, 0, "press <C-c> to quit\n"});
    commands.push_back({Command::quit, 0, 0, ""});

    // setup scheduler and run
    auto scheduler_ptr = std::make_shared<Monitor<FCFSScheduler>>();
    scheduler_ptr->unsafe_get_access()->create(0, commands, 0);
    cpu.run(scheduler_ptr);
    cpu.thread_handle.join();

    return 0;
}

