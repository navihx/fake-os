#include "MemoryApi.h"

int main(int argc, char **argv){

    if(argc < 4){
        std::cout << "Memory Demo" << std::endl
                  << "USAGE:" <<std::endl
                  << "memory-demo <size> <count> <address1> <address2> ……" << std::endl;
        return 0;
    }

    //获取帧/页大小
    int frame_size = std::atoi(argv[1]);
    
    //帧数
    int frame_num = std::atoi(argv[2]);

    //初始化内存
    MemoryApi::initial_memory(frame_size,frame_num);

    //新建一个页表
    PageTable page_table;

    //全部的虚拟地址
    for(int i = 3;i < argc;i++){
        long long logical_loc = 0;
        if(argv[i][1] == 'b'){
            //以0b开头
            argv[i][1] = '0';
            logical_loc = std::strtoll(argv[i],nullptr,2);
        }else{
            logical_loc = std::strtoll(argv[i],nullptr,0);
        }        
        //直接access全部的虚拟地址
        if(!MemoryApi::access_memory(&page_table,logical_loc)){
            //未能成功access 说明没有加载入内存
            std::cout << "["<< (logical_loc >> g_offset_n) <<"] : page fault, load, ";
            MemoryApi::load_into_memory(&page_table,logical_loc);
            //load完之后再次确认是否进入内存
            if(MemoryApi::is_in_memory(&page_table,logical_loc)){
                std::cout <<"load success" << std::endl;
            }else{
                std::cout << "load fail" << std::endl;
            }
        }else{
            std::cout << "["<< (logical_loc >> g_offset_n) <<"] : acess success" << std::endl;
        }
    }

}