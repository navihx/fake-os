#include "cpu.hpp"
#include "PCB.hpp"
#include "scheduler.h"
#include "MemoryApi.h"

int main()
{
    std::vector<Command> comm1 = {
        {Command::wait,1,0,""},
        {Command::print,0,0,"p1 print\n"},
        {Command::signal,1,0,""},
        {Command::sleep,0,0,""},
        {Command::jump,0,-5,""},
    };

    std::vector<Command> comm2 = {
        {Command::wait,1,0,""},
        {Command::print,0,0,"p2 print\n"},
        {Command::signal,1,0,""},
        {Command::sleep,0,0,""},
        {Command::jump,0,-5,""},
    };

    MemoryApi::initial_memory(1024, 10);
    std::shared_ptr<Monitor<FCFSScheduler>> scheduler_p=std::make_shared<Monitor<FCFSScheduler>>();
 
    scheduler_p->unsafe_get_access()->create(0,comm1,0);
    scheduler_p->unsafe_get_access()->create(0,comm2,0);

    CPU cpu;

    cpu.set_id(1);
    if(cpu.get_id()==1)
        cpu.run(scheduler_p);
    cpu.thread_handle.join();

    return 0;
}
