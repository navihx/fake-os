#include <memory>
#include <string>
#include <vector>
#include "MemoryApi.h"
#include "cpu.hpp"
#include "PCB.hpp"
#include "interupt.hpp"
#include "peripheral.h"
#include "scheduler.h"
#include "sync-queue.hpp"

int main() {
    CPU cpu;
    std::vector<std::shared_ptr<Peripheral>> p = { std::make_shared<SSTFPeripheral>() };
    auto scheduler_p = std::make_shared<Monitor<FCFSScheduler>>();
    auto g_interrupt = std::make_shared<SyncQueue<Interupt>>();
    MemoryApi::initial_memory(1024, 100);

    std::vector<Command> commands = {};
    for(int i=0;i<10;i++) {
        commands.push_back(Command{Command::use, 0, 1, ""});
        commands.push_back(Command{Command::print, 0, 0, "IO: "+ std::to_string(i) +" completed\n"});
    }
    commands.push_back(Command{Command::print, 0, 0,"press <C-c> to quit\n"});
    commands.push_back(Command{Command::quit, 0, 0, ""});

    for (auto pp: p) {
        pp->run(g_interrupt);
    }

    scheduler_p->unsafe_get_access()->create(0, commands, 0);
    cpu.global_interupt_queue = g_interrupt.get();
    cpu.run(scheduler_p, p);
    cpu.thread_handle.join();

    return 0;
}

