#include "peripheral.h"
#include <cstring>
#include <memory>
#include <iostream>

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        std::cout << "SSTF Demo" << std::endl
                  << "USAGE:" << std::endl
                  << "SSTF-demo start_position track.." << std::endl;
    }

    SSTFPeripheral *p = new SSTFPeripheral;
    p->seek_start = std::atoi(argv[1]);
    auto callback = std::make_shared<SyncQueue<Interupt>>();

    for (int i = 2; i < argc; i++)
    {
        p->io_queue.push(IORequest{0, 0, 0, std::atoi(argv[i]), IORequest::Type::REQUEST});
    }

    p->run(callback);

    for (int i = 2; i < argc; i++)
    {
        auto tmp = callback->pop();
        std::cout << "Peripherals serve: " << tmp->arg4 << std::endl;
    }

    p->io_queue.push(IORequest{0, 0, 0, 0, IORequest::Type::SHUTDOWN});
    p->thread_handle.join();
    return 0;
}